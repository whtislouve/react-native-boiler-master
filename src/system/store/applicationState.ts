import { ISystemState } from 'app/system/store/system'
import { ILoginState } from 'app/module/login/store/loginState'

export interface IApplicationState {
  system: ISystemState
  login: ILoginState
}