export const fonts = {
    geometria: 'Geometria',
    geometriaBold: 'Geometria-Bold',
    geometriaExtraBold: 'Geometria-ExtraBold',
    geometriaHeavy: 'Geometria-Heavy',
    geometriaItalic: 'Geometria-Italic',
    geometriaLight: 'Geometria-Light',
    geometriaMedium: 'Geometria-Medium',
    geometriaThin: 'Geometria-Thin',
    openSansRegular: 'OpenSans-Regular',
}