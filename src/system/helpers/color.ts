export enum Color {
    white = '#FFFFFF',
    darkCharcoal = '#2F2F2F',
    graniteGray = '#626262',
    americanSliver = '#D0D0D0',
    mediumSeaGreen = '#2CB76C',
    royalPurple = '#7A4D9F',
    chineseSilver = '#CDCDCD',
    spanishGray = '#9C9C9C',
    gainsBoro = '#DCDCDC',
    antiFlashWhite = '#F1F1F1',
}
