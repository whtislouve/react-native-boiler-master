import React from 'react'
import{
  styleSheetCreate,
  style,
  windowWidth,
} from 'app/system/helpers'
import { createStackNavigator } from '@react-navigation/stack'
import { ListPages  } from 'app/system/navigation'
import { Authorisation } from 'app/module/login/view/Authorisation'
import { AccessCode } from 'app/module/login/view/AccessCode'
import { MainPage } from 'app/module/mainPage/view/MainPage'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { CommonDrawer } from 'app/module/global/view/CommonDrawer'

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

/*export const RootNavigator = (): JSX.Element => {
  return (
    <Stack.Navigator headerMode={undefined}>
      <Stack.Screen
        name={ListPages.Login}
        component={Authorisation}
      />
      <Stack.Screen
        name={ListPages.AccessCode}
        component={AccessCode}
      />
      <Stack.Screen
        name={ListPages.MainPage}
        component={MainPage}
      />
    </Stack.Navigator>
  )
}*/


export const RootNavigator = (): JSX.Element => {
  return (
    <Drawer.Navigator
      drawerStyle={styles.drawer}
      drawerContent={customDrawer}
      initialRouteName="AccessCode"
      >
      {/* <Drawer.Screen
        name={ListPages.MainPage}
        component={MainPage}
      /> */}
      <Drawer.Screen
        name={ListPages.Login}
        component={Authorisation}
      />
      <Drawer.Screen
        name={ListPages.AccessCode}
        component={AccessCode}
      />
    </Drawer.Navigator>
  )
}

const customDrawer  = (props) => {
  return (
    <CommonDrawer {...props}/>
  )
}

const styles = styleSheetCreate({
  drawer: style.view({
    width: windowWidth * 0.8,
  }),
})
