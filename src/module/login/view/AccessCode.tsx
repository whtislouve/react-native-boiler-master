import React, { PureComponent } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Switch,
  StyleSheet,
  Alert,
} from 'react-native'
import { ImageRepository } from 'app/system/helpers'
import { style } from 'app/system/helpers'
import { windowWidth } from 'app/system/helpers'
import { fonts } from 'app/system/helpers'
import { Color } from 'app/system/helpers'
import { CommonInput } from 'app/module/global/view/CommonInput'
import { CommonButton } from 'app/module/global/view/CommonButton'
import { asyncActionCreator, connectStore, IApplicationState } from 'app/system/store'
import { ThunkDispatch } from 'redux-thunk'
import { ILoginState } from '../store/loginState'
import { loginAsyncActions } from '../store/loginAsyncActions'
import { Loader } from 'app/module/global/view/Loader'



interface IProps {
  navigation: any
}

interface IState {
  accessCode: string
}

interface IStateProps {
  isLoading: boolean
  error: boolean
}

interface IDispatchProps{
  accessCode(data: IAccessCodeRequest):Promise<void>
}

@connectStore(
  (state: IApplicationState): IStateProps => ({
    isLoading: state.login.isLoading,
    error: state.login.error
  }),
  (dispatch: ThunkDispatch<ILoginState, void, any>): IDispatchProps => ({
    async accessCode(data) {
      await dispatch(loginAsyncActions.accessCode(data))
    }
  })
)

export class AccessCode extends PureComponent<IProps &  IState & IStateProps & IDispatchProps> {
  state = {
    switchValue: true,
    accessCode: '',
  }

  onChangeAccessCode = (accessCode: string) =>{
    this.setState({accessCode})
  }

  toggleSwitch = () => {
    this.setState({ switchValue: !this.state.switchValue })
  }

  Back = () => {
    this.props.navigation.navigate('Login')
  }

  onEnterButton = () => {
    this.props.navigation.navigate('MainPage')
  }
  
  onSubmitHandler = async() => {
    await this.props.accessCode({
      code: this.props.accessCode
    })
    if(this.props.isLoading) {
      setTimeout(() => {
        Alert.alert("Ошибка")
      }, 400)
    }
  }

  render() {
    if(this.props.isLoading){
      return <Loader />
    }
    return(
      <View style={styles.container}>
        <View>
          <Image
            source={ImageRepository.logo}
            style={styles.logo}
          />
        </View>
        <Text style={styles.authorisationWord}>
          Код доступа
        </Text>
        <Text style={styles.words}>
          Введите код доступа ниже, {"\n "}и войдите в личный кабинет
        </Text>
        <View>
          <CommonInput
            label='Код доступа'
            containerStyle={styles.loginContainer}
            onChangeText={this.onChangeAccessCode}
            value={this.state.accessCode}
          />
        </View>
        <View style={styles.buttonContainer}>
          <View style={styles.buttons}>
            <CommonButton
              label='Войти'
              color='PURPLE'
              onPress={this.onSubmitHandler}
            />
          </View>
        </View>
        <View style={styles.politTextContainer}>
          <View style={styles.switchContainer}>
            <Switch
              onValueChange={this.toggleSwitch}
              value={this.state.switchValue}
              activeText={''}
              inActiveText={''}
              barHeight={windowWidth * 0.049}
              circleSize={windowWidth * 0.069}
              backgroundActive={Color.mediumSeaGreen}
              circleActiveColor={Color.mediumSeaGreen}
            />
            <Text style={styles.politText}>
              Согласен на обработку персональных данных {"\n"}
              и принимаю условия Политики конфиденциальности.
            </Text>
          </View>
        </View>
        <View style={styles.containerNav}>
          <View style={styles.buttonsNav}>
            <TouchableOpacity onPress={this.Back}>
              <Text style={styles.textNavBack}>
                НАЗАД
              </Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text style={styles.textNavNext}>
                ДАЛЕЕ
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: style.view({
    alignItems: 'center',
    backgroundColor: Color.white,
    flex: 1,
    paddingHorizontal: windowWidth * 0.04,
  }),
  logo: style.image({
    width: windowWidth * 0.93,
    height: windowWidth * 0.21,
    marginTop:  windowWidth * 0.18,
  }),
  authorisationWord: style.text({
    color: Color.darkCharcoal,
    fontSize: windowWidth * 0.096,
    fontFamily: fonts.geometriaBold,
    marginTop: windowWidth * 0.06,
  }),
  words: style.text({
    color: Color.darkCharcoal,
    textAlign: 'center',
    fontSize: windowWidth * 0.048,
    fontFamily: fonts.geometriaLight,
    marginTop: windowWidth * 0.046,
  }),
  loginContainer: style.view({
    marginTop: windowWidth * 0.1,
  }),
  passwordContainer: style.view({
    marginTop: windowWidth * 0.08,
  }),
  accessContainer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: windowWidth * 0.05,
    width: '100%',
  }),
  accessText: style.text({
    marginRight: windowWidth * 0.1,
    marginLeft: windowWidth * 0.05,
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaLight,
  }),
  questionMark: style.image({
    width: windowWidth * 0.05,
    height: windowWidth * 0.05,
    marginRight: windowWidth * 0.25,
  }),
  forgotPassword: style.text({
    fontSize: windowWidth * 0.04,
    marginRight: windowWidth * 0.05,
    color: Color.mediumSeaGreen,
    fontFamily: fonts.geometriaLight,
  }),
  dottedLine: style.image({
    position: 'absolute',
    left: windowWidth * 0.15,
    width: windowWidth * 0.3,
    height: windowWidth * 0.003,
  }),
  buttons: style.view({
    alignSelf: 'flex-end',
  }),
  buttonContainer:style.view({
    width: '100%',
    marginTop: windowWidth * 0.06,
  }),
  politTextContainer: style.view({
    width: '100%',
    marginTop: windowWidth * 0.1,
    marginRight: windowWidth * 0.05,
    marginLeft: windowWidth * 0.04,
  }),
  switchContainer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
  }),
  politText: style.text({
    fontSize: windowWidth * 0.029,
    fontFamily: fonts.geometriaLight,
    textDecorationLine: 'underline',
  }),
  containerNav: style.view({
    height: '100%',
    width: '100%',
  }),
  buttonsNav: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: windowWidth * 0.2,
    paddingLeft: windowWidth * 0.02,
  }),
  textNavBack: style.text({
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaBold,
    color: Color.darkCharcoal,
  }),
  textNavNext: style.text({
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaBold,
    color: Color.chineseSilver,
  }),
})
