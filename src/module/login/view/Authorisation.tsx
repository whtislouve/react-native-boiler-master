import React, { PureComponent } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Switch,
  Alert,
  ScrollView,
} from 'react-native'
import { ImageRepository } from 'app/system/helpers'
import { style } from 'app/system/helpers'
import { windowWidth } from 'app/system/helpers'
import { fonts } from 'app/system/helpers'
import { Color } from 'app/system/helpers'
import { CommonInput } from 'app/module/global/view/CommonInput'
import { CommonButton } from 'app/module/global/view/CommonButton'
import Modal from 'react-native-modal'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { connect } from 'react-redux'
import { connectStore, IApplicationState } from 'app/system/store'
import { ThunkDispatch } from 'redux-thunk'
import { loginAsyncActions } from '../store/loginAsyncActions'



interface IProps {
  navigation: any
}

interface IState {
  login: string
  password: string
}

interface IStateProps {

}

interface IDispatchProps {
  login(data: ILoginRequest): Promise<void>
}

@connectStore(
  null,
  (dispatch: ThunkDispatch<IApplicationState, void, any>): IDispatchProps => ({
    async login(data) {
      await dispatch(loginAsyncActions.login(data))
    }
  })
)


export class Authorisation extends PureComponent<IProps & IState & IDispatchProps & IStateProps>{
  state = {
    switchValue: true,
    switchModal: false,
    login: '',
    password: '',
  }
  
  toggleSwitch = () => {
    this.setState({ switchValue: !this.state.switchValue })
  }

  onSubmitHandler = async() => {
    await this.props.login({
      login: this.props.login,
      hash: this.props.password
    })
  }

  toggleModal = () => {
    this.setState({ switchModal: !this.state.switchModal })
  }

  onChange = () => {
    Alert.alert("АСУ")
  }

  onChangeLogin = (login: string) =>{
    this.setState({login})
  }

  onChangePassword = (password: string) => {
    this.setState({password})
  }

  onChangeScreen = () => {
    this.setState({switchModal: false}, () =>
      setTimeout(() => {this.props.navigation.navigate('AccessCode')},500)
    )
  }

  render(){
    return (
      <KeyboardAwareScrollView contentContainerStyle={styles.container}>
        <View>
          <Image
            source={ImageRepository.logo}
            style={styles.logo}
          />
        </View>
        <Text style={styles.authorisationWord}>
          Авторизация
        </Text>
        <Text style={styles.words}>
          Введите логин и пароль, {"\n "}полученные при регистрации
        </Text>
        <View>
          <CommonInput
            label='Логин'
            containerStyle={styles.loginContainer}
            onChangeText={this.onChangeLogin}
            value={this.state.login}
          />
          <CommonInput
            label='Пароль'
            containerStyle={styles.passwordContainer}
            onChangeText={this.onChangePassword}
            value={this.state.password}
          />
        </View>
        <View style={styles.accessContainer}>
          <Text style={styles.accessText}>
            Есть код доступа?
          </Text>
          <TouchableOpacity>
            <Image
              style={styles.questionMark}
              source={ImageRepository.questionMark}
            />
          </TouchableOpacity>
          <Text style={styles.forgotPassword}>
            Забыл пароль?
          </Text>
        </View>
        <View >
          <Image
            style={styles.dottedLine}
            source={ImageRepository.dottedLine}/>
        </View>
        <View style={styles.buttonContainer}>
         <View style={styles.buttons}>
            <CommonButton
              label='Регистрация'
            />
            <CommonButton
              label='Войти'
              color='PURPLE'
              onPress={this.onSubmitHandler}
            />
          </View>
        </View>
        <View style={styles.politTextContainer}>
          <View style={styles.switchContainer}>
            <Switch
              onValueChange={this.toggleSwitch}
              value={this.state.switchValue}
              activeText={''}
              inActiveText={''}
              barHeight={windowWidth * 0.049}
              circleSize={windowWidth * 0.069}
              backgroundActive={Color.mediumSeaGreen}
              circleActiveColor={Color.mediumSeaGreen}
            />
            <Text style={styles.politText}>
              Согласен на обработку персональных данных {"\n"}
              и принимаю условия Политики конфиденциальности.
            </Text>
          </View>
        </View>
        <Modal isVisible={this.state.switchModal}>
          <View style={styles.containerModal}>
            <View style={styles.headerModal}>
              <Image
                style={styles.questionMarkModal}
                source={ImageRepository.questionMarkModal}
              />
              <Text style={styles.headerTextModal}>
                Неверный пароль
              </Text>
              <TouchableOpacity onPress={this.toggleModal}>
                <Image
                  style={styles.closeModal}
                  source={ImageRepository.closeModal}
                />
              </TouchableOpacity>
            </View>
            <Image
              style={styles.lineModal}
              source={ImageRepository.lineModal}
            />
            <View style={styles.textContainer}>
              <Text style={styles.textContent}>
                Вы ввели неверный пароль,{"\n"}
                попробуйте снова
              </Text>
            </View>
            <View style={styles.containerPasswordModal}>
              <View style={styles.passwordModal}>
                <TouchableOpacity
                  onPress={this.onChangeScreen}
                >
                <Text style={styles.textPasswordModal}>
                  Ввести пароль
                </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </KeyboardAwareScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: style.view({
    alignItems: 'center',
    backgroundColor: Color.white,
    flex: 1,
    paddingHorizontal: windowWidth * 0.04,
  }),
  logo: style.image({
    width: windowWidth * 0.93,
    height: windowWidth * 0.21,
    marginTop:  windowWidth * 0.1,
  }),
  authorisationWord: style.text({
    color: Color.darkCharcoal,
    fontSize: windowWidth * 0.096,
    fontFamily: fonts.geometriaBold,
    marginTop: windowWidth * 0.06,
  }),
  words: style.text({
    color: Color.darkCharcoal,
    textAlign: 'center',
    fontSize: windowWidth * 0.048,
    fontFamily: fonts.geometriaLight,
    marginTop: windowWidth * 0.046,
  }),
  loginContainer: style.view({
    marginTop: windowWidth * 0.1,
  }),
  passwordContainer: style.view({
    marginTop: windowWidth * 0.08,
  }),
  accessContainer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: windowWidth * 0.05,
    width: '100%',
  }),
  accessText: style.text({
    marginRight: windowWidth * 0.1,
    marginLeft: windowWidth * 0.05,
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaLight,
  }),
  questionMark: style.image({
    width: windowWidth * 0.05,
    height: windowWidth * 0.05,
    marginRight: windowWidth * 0.25,
  }),
  forgotPassword: style.text({
    fontSize: windowWidth * 0.04,
    marginRight: windowWidth * 0.05,
    color: Color.mediumSeaGreen,
    fontFamily: fonts.geometriaLight,
  }),
  dottedLine: style.image({
    position: 'absolute',
    left: windowWidth * 0.15,
    width: windowWidth * 0.3,
    height: windowWidth * 0.003,
  }),
  buttons: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
  }),
  buttonContainer:style.view({
    width: '100%',
    marginTop: windowWidth * 0.06,
  }),
  politTextContainer: style.view({
    width: '100%',
    marginTop: windowWidth * 0.1,
    marginRight: windowWidth * 0.05,
    marginLeft: windowWidth * 0.04,
  }),
  switchContainer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
  }),
  politText: style.text({
    fontSize: windowWidth * 0.029,
    fontFamily: fonts.geometriaLight,
    textDecorationLine: 'underline',
  }),
  containerModal: style.view({
    backgroundColor: Color.white,
    width: windowWidth * 0.87,
    height: windowWidth * 0.53,
    position: 'absolute',
    top: windowWidth * 0.57,
    left: windowWidth * 0.015,
  }),
  headerModal: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: windowWidth * 0.04,
    marginTop: windowWidth * 0.05,
  }),
  questionMarkModal: style.image({
    width: windowWidth * 0.064,
    height: windowWidth * 0.064,
  }),
  headerTextModal: style.text({
    textAlign: 'left',
    fontSize: windowWidth * 0.05,
    fontFamily: fonts.geometriaBold,
    marginRight: windowWidth * 0.1,
  }),
  closeModal: style.image({
    width: windowWidth * 0.05,
    height: windowWidth * 0.05,
  }),
  lineModal: style.image({
    marginTop: windowWidth * 0.048,
    width: '100%',
  }),
  textContainer: style.view({
    height: windowWidth * 0.2,
  }),
  textContent: style.text({
    position: 'absolute',
    left: windowWidth * 0.067,
    top: windowWidth * 0.054,
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaLight,
    color: Color.darkCharcoal,
  }),
  passwordModal: style.view({
    width: windowWidth * 0.87,
    height: windowWidth * 0.165,
    backgroundColor: 'green',
  }),
  containerPasswordModal: style.view({
    position: 'absolute',
    bottom: 0
  }),
  textPasswordModal: style.text({
    alignSelf: 'center',
    paddingVertical: windowWidth * 0.05,
    fontSize: windowWidth * 0.039,
    fontFamily: fonts.geometriaBold,
    color: Color.white,
  }),
})
