import { ApiService } from 'app/system/api'

export function loginRequest(params: ILoginRequest): Promise<IUserInformation> {
    return ApiService.get(
        'https://houseb.ru/api/api.php?action=authentication',
        { params }
    )
}

export function accessCodeRequest(params: IAccessCodeRequest): Promise<IUserInformation> {
    return ApiService.get(
        'https://houseb.ru/api/api2.php?action=authentication',
        { params }
    )
}