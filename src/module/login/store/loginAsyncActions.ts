import { asyncActionCreator } from 'app/system/store/actionCreator'
import { accessCodeRequest, loginRequest } from '../api/request'

export class loginAsyncActions {
    static login = asyncActionCreator<ILoginRequest, IUserInformation, Error>(
        'LOGIN/LOGIN',
        loginRequest
    )
    static accessCode = asyncActionCreator<IAccessCodeRequest, IUserInformation, Error>(
        'LOGIN/ACCESSCODE',
        accessCodeRequest
    )
}