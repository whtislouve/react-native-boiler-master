import { ReducerBuilder, reducerWithInitialState } from "typescript-fsa-reducers";
import { loginAsyncActions } from "./loginAsyncActions";
import { ILoginState, loginInititalState } from "./loginState";

const loginStarted = (state: ILoginState): ILoginState => {
    return{
        ...state,
        isLoading:true,
    }
}

const loginDone = (state: ILoginState, payload: any): ILoginState => {
    if(payload.result.status === 1) {
        return{
            ...state,
            isLoading: false,
            error: true, 
        }
    }
    return{
        ...state,
        isLoading: false,
        error: false,
    }
}

const loginFailed = (state: ILoginState): ILoginState => {
    return{
        ...state,
        isLoading:false,
        error: true,
    }
}

const accessCodeStarted = (state: ILoginState): ILoginState => {
    return{
        ...state,
        isLoading:true,
    }
}

const accessCodeDone = (state: ILoginState, payload: any): ILoginState => {
    console.log(payload)
    if(payload.result.status === 1) {
        return{
            ...state,
            isLoading: false,
            error: true, 
        }
    }
    return{
        ...state,
        isLoading: false,
        error: false,
    }
}

const accessCodeFailed = (state: ILoginState): ILoginState => {
    return{
        ...state,
        isLoading:false,
        error: true,
    }
}

export const loginReducer: ReducerBuilder<ILoginState> = reducerWithInitialState(loginInititalState)
    .case(loginAsyncActions.login.async.started, loginStarted)
    .case(loginAsyncActions.login.async.done, loginDone)
    .case(loginAsyncActions.login.async.failed, loginFailed)
    .case(loginAsyncActions.accessCode.async.started, accessCodeStarted)
    .case(loginAsyncActions.accessCode.async.done, accessCodeDone)
    .case(loginAsyncActions.accessCode.async.failed, accessCodeFailed)
