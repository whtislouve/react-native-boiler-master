export interface ILoginState {
    isLoading: boolean
    error: boolean
}

export const loginInititalState: ILoginState = {
    isLoading: false,
    error: false,
}
