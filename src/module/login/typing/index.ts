interface ILoginRequest { 
    login: string,
    hash: string,
}

interface IUserInformation {
    
}

interface IAccessCodeRequest {
    code: string,
}