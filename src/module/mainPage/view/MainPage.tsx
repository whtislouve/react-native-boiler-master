import React, { PureComponent, Fragment } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native'
import {
  Color,
  fonts,
  ImageRepository,
  style,
  windowWidth,
} from 'app/system/helpers'
import { Header } from 'app/module/global/view/Header'
import {DrawerNavigationProp} from '@react-navigation/drawer'



interface IProps {
  navigation: DrawerNavigationProp<any>
}

interface IState {

}

export class MainPage extends PureComponent<IProps, IState> {
  render() {
    return(
      <Fragment>
        <Header
          isMainHeader
          navigation={this.props.navigation}
        />
        <View style={styles.superContainer}>
          <ScrollView>
            <View style={styles.buildingContainer}>
              <View style={styles.building}>
                <Image
                  style={styles.buildingImage}
                  source={ImageRepository.building}/>
                <View>
                  <Text style={styles.buildingType}>
                    ПЖСК не продано
                  </Text>
                  <Text style={styles.buildingAddress}>
                    Молодежная д.4 кв.416
                  </Text>
                  <Text style={styles.buildingAccount}>
                    Лицевой счет
                  </Text>
                  <Text style={styles.buildingAccountNumbers}>
                    0000004416
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.personalAccount}>
              <View style={styles.header}>
                <View>
                  <Text style={styles.headerText}>
                    Ваш лицевой счёт
                  </Text>
                  <Text style={styles.headerInfo}>
                    (информация на Март 2020)
                  </Text>
                </View>
                <Image
                  style={styles.lineStyle}
                  source={ImageRepository.lineMainPage}
                />
                <View style={styles.monthlyCharge}>
                  <Text style={styles.monthlyChargeText}>
                    Начисление за{'\n'}текущий месяц:
                  </Text>
                  <Text style={styles.monthlyChargeAmount}>
                    1123.46 руб.
                  </Text>
                </View>
                <Image
                  style={styles.lineStyle}
                  source={ImageRepository.lineMainPage}
                />
                <View style={styles.monthPay}>
                  <Text style={styles.monthlyChargeText}>
                    Оплата в месяце
                  </Text>
                  <Text style={styles.monthPayAmount}>
                    0 руб.
                  </Text>
                </View>
                <Image
                  style={styles.lineStyle}
                  source={ImageRepository.lineMainPage}
                />
                <View style={styles.debt}>
                  <Text style={styles.debtText}>
                    Долг:
                  </Text>
                  <Text style={styles.debtAmount}>
                    3254.38 руб.
                  </Text>
                </View>
              </View>
            </View>

            <View style={styles.containerPayList}>
              <View style={styles.payList}>
                <View>
                  <Text style={styles.headerPayList}>
                    Ваш список оплат
                  </Text>
                  <Image
                    style={styles.lineStylePayList}
                    source={ImageRepository.lineMainPage}
                  />
                </View>
                <View style={styles.monthlyCharge}>
                  <Text style={styles.payListText}>
                    03.04.2020
                  </Text>
                  <Text style={styles.monthlyChargeAmount}>
                    22 854.31 руб.
                  </Text>
                </View>
              </View>
            </View>

            <View style={styles.footerContainer}>
              <View style={styles.footer}>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.billImage}
                    source={ImageRepository.bills}/>
                  <Text style={styles.footerText}>
                    Счета
                  </Text>
                </View>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.countersImage}
                    source={ImageRepository.counters}/>
                  <Text style={styles.footerText}>
                    Счётчики
                  </Text>
                </View>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.requestImage}
                    source={ImageRepository.requestListMain}/>
                  <Text style={styles.footerText}>
                    Заявки
                  </Text>
                </View>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.voteImage}
                    source={ImageRepository.votes}/>
                  <Text style={styles.footerText}>
                    Голосования
                  </Text>
                </View>
                <View style={styles.imageContainer}>
                  <Image
                    style={styles.billImage}
                    source={ImageRepository.calling}/>
                  <Text style={styles.footerText}>
                    Позвонить
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </Fragment>
    )
  }
}

const styles = StyleSheet.create({
  buildingContainer: style.view({
    paddingHorizontal: windowWidth * 0.048,
    paddingTop: windowWidth * 0.04,
  }),
  building: style.view({
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: Color.white,
    borderRadius: windowWidth * 0.01,
    height: windowWidth * 0.45,
    paddingHorizontal: windowWidth * 0.04,
  }),
  buildingImage: style.image({
    width: windowWidth * 0.29,
    height: windowWidth * 0.29,
  }),
  buildingType: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.05,
  }),
  buildingAddress: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.044,
    fontFamily: fonts.openSansRegular,
  }),
  buildingAccount: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaLight,
  }),
  buildingAccountNumbers: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.044,
    fontFamily: fonts.geometriaBold,
  }),
  superContainer: style.view({
    height: '100%',
    flex: 1,
  }),
  personalAccount: style.view({
    paddingHorizontal: windowWidth * 0.048,
    paddingTop: windowWidth * 0.04,
  }),
  header: style.view({
    backgroundColor: Color.white,
    paddingLeft: windowWidth * 0.048,
    borderRadius: windowWidth * 0.01,
  }),
  headerText: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.07,
    paddingTop: windowWidth * 0.05,
  }),
  headerInfo: style.text({
    paddingTop: windowWidth * 0.05,
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    color: Color.graniteGray,
    marginBottom: windowWidth * 0.03,
  }),
  lineStyle: style.image({
    width: windowWidth * 0.9,
    alignSelf: 'flex-end',
    height: windowWidth * 0.007,
  }),
  monthlyCharge: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: windowWidth * 0.03,
    marginBottom: windowWidth * 0.028,
  }),
  monthlyChargeText: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
  }),
  monthlyChargeAmount: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    color: Color.mediumSeaGreen,
    marginRight: windowWidth * 0.09,
  }),
  monthPay: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: windowWidth * 0.048,
    marginBottom: windowWidth * 0.05,
  }),
  monthPayAmount: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    color: Color.mediumSeaGreen,
    marginRight: windowWidth * 0.2,
  }),
  debt: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: windowWidth * 0.048,
    marginBottom: windowWidth * 0.05,
  }),
  debtText: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    color: Color.mediumSeaGreen,
  }),
  debtAmount: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    color: Color.mediumSeaGreen,
    marginRight: windowWidth * 0.07,
  }),
  containerPayList: style.view({
    paddingHorizontal: windowWidth * 0.048,
    marginTop: windowWidth * 0.04,
    height: windowWidth * 0.35,
    marginBottom: windowWidth * 0.25,
  }),
  payList: style.view({
    backgroundColor: Color.white,
    height: '100%',
    paddingHorizontal: windowWidth * 0.048,
    borderRadius: windowWidth * 0.01,
  }),
  headerPayList: style.text({
    paddingVertical: windowWidth * 0.06,
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.07,
  }),
  lineStylePayList: style.image({
    width: windowWidth * 0.9,
    alignSelf: 'center',
    height: windowWidth * 0.007,
  }),
  payListText: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.04,
    marginRight: windowWidth * 0.31,
  }),
  footer: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: windowWidth * 0.06,
    paddingRight: windowWidth * 0.06,
    paddingTop: windowWidth * 0.04,
  }),
  footerContainer: style.view({
    position: 'absolute',
    bottom: 0,
    backgroundColor: Color.white,
    height: windowWidth * 0.21,
    width: '100%',
  }),
  imageContainer: style.view({
    alignItems: 'center',
  }),
  billImage:style.image({
    width: windowWidth * 0.062,
    height: windowWidth * 0.069,
  }),
  countersImage: style.image({
    width: windowWidth * 0.069,
    height: windowWidth * 0.069,
  }),
  requestImage: style.image({
    width: windowWidth * 0.082,
    height: windowWidth * 0.07,
  }),
  voteImage: style.image({
    width: windowWidth * 0.081,
    height: windowWidth * 0.07,
  }),
  footerText: style.text({
    paddingTop: windowWidth * 0.03,
    fontSize: windowWidth * 0.033,
    fontFamily: fonts.geometriaBold,
  }),
})
