import React, { PureComponent, Fragment } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  ScrollView,
  Animated,
  TouchableOpacity,
  ImageBackground,
  ImageURISource,
} from 'react-native'
import {
  Color, 
  fonts,
  ImageRepository,
  style, 
  windowWidth,
  styleSheetFlatten,
} from 'app/system/helpers'

interface IProps {
  navigation?: any
}

interface IState {
  isAccountsPointsOpen: boolean
  animatedHeightAccountPoints: Animated.Value
}

interface IMenuItems {
  image: ImageURISource
  stylesImage: Object
  text: string
  routeName: string
}

const accountsPoints = [
  {
    name: 'Ваш лицевой счёт', 
  },
  
  {
    name: 'Подробности\nначислений', 
  },
  
  {
    name: 'История начислений', 
  },

]

const accountsListItemHight = windowWidth * 0.14
const accountsListHight = accountsListItemHight * 3

export class CommonDrawer extends PureComponent <IProps, IState> {

  state = {
    isAccountsPointsOpen: false,
    animatedHeightAccountPoints: new Animated.Value(0), // анимационноя переменная
  }

  openAccountPoints = (): void => {
    Animated.timing(this.state.animatedHeightAccountPoints, {
      toValue: 1,
      duration: 500,
      useNativeDriver: false,
    })
      .start()
  }

  closeAccountPoints = (): void => {
    Animated.timing(this.state.animatedHeightAccountPoints, {
      toValue: 0,
      duration: 500,
      useNativeDriver: false,
    })
      .start()
  }

  toggleAccountPoints = (): void => {
    if (this.state.isAccountsPointsOpen) {
      this.setState({ isAccountsPointsOpen: false },  this.closeAccountPoints )
    } else {
      this.setState({ isAccountsPointsOpen: true },  this.openAccountPoints )
    }
  }

  render() {
    const accountPoints = styleSheetFlatten([
      styles.accountsList,
      {
        height: this.state.animatedHeightAccountPoints.interpolate({
          inputRange: [0, 1],
          outputRange: [0, accountsListHight],
        })
      }
    ])

    const arrowAccountPoint = styleSheetFlatten([
      styles.arrowAccountImage,
      {
        transform: [
          {
            rotate: this.state.animatedHeightAccountPoints.interpolate({
              inputRange: [0, 1],
              outputRange: ['0deg', '180deg'],
            })
          }
        ]
      }
    ])

    return(
      <Fragment>
        <View style={styles.headerContainer}>
          <ImageBackground 
            style={styles.backHeaderImage}
            source={ImageRepository.menu}
            >
            <View style={styles.header}>
              <View>
                <Text style={styles.buildingType}>
                  ПЖСК не продано
                </Text>
                <Text style={styles.buildingAddress}>
                  Молодежная д.4 кв.416
                </Text>
              </View>
              <View style={styles.headerInfoAccount}>
                <View>
                  <Text style={styles.buildingAccount}>
                    Лицевой счет
                  </Text>
                  <Text style={styles.buildingAccountNumbers}>
                    0000004416
                  </Text>
                </View>
                <TouchableOpacity style={styles.itemsBackHeader}>
                  <Image 
                    style={styles.backArrowImage}
                    source={ImageRepository.backArrow}/>
                  <Text style={styles.headerText}>
                    Обратно
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>

        <View style={styles.contentContainer}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            bounces={false}
          >
            <View style={styles.interviewContainer}>
              <TouchableOpacity 
                onPress={this.toggleAccountPoints}
                style={styles.content}>
                <Image
                  style={styles.billsImage}
                  source={ImageRepository.bills}
                />
                <Text style={styles.contentText}>
                  Счета
                </Text>
                <Animated.Image
                  source={ImageRepository.arrowAccountPoint}
                  style={arrowAccountPoint}
                >
                </Animated.Image>
              </TouchableOpacity>
            </View>

            <Animated.View style={accountPoints}>
              {
                accountsPoints.map(point => {
                  return (
                    <View 
                      style={styles.itemsSubparagraphList}
                      key={point.name}>
                      <Text style={styles.itemsSubparagraphText}>
                        {point.name}
                      </Text>
                    </View>
                  )
                })
              }
            </Animated.View>

            <View >
              {
                drawerList.map(items => {
                  return (
                    <TouchableOpacity 
                      key={Math.random.toString()}
                      onPress={() => {this.props.navigation.navigate(items.routeName)}}
                      style={styles.interviewContainer}>
                      <View style={styles.content}>
                        <Image
                          style={items.stylesImage}
                          source={items.image}
                        />
                        <Text style={styles.contentTextCounters}>
                          { items.text }
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )
                })
              }
            </View>
          </ScrollView>

          <View style={styles.footerContainer}>
            <TouchableOpacity>
              <View style={styles.footer}>
                <Image
                  style={styles.exitAccountImage}
                  source={ImageRepository.exitAccount}
                />
                <View style={styles.footerTextView}>
                  <Text style={styles.footerExit}>
                    Выход из учетной {"\n"}записи
                  </Text>
                  <Text style={styles.footerVersion}>
                    Версия программы: 317.11.46.11 от 13.11.2017
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
      </View>
    </Fragment>
    )
  }
}

const styles = StyleSheet.create({
  container: style.view({
    height: '100%',
  }),
  headerContainer: style.view({
    backgroundColor: Color.mediumSeaGreen,
    height: windowWidth * 0.42,
  }),
  header: style.view({
    paddingLeft: windowWidth * 0.04,
    paddingTop: windowWidth * 0.027,
  }),
  backHeaderView: style.view({
    position: 'absolute',
    bottom: 0,
    left: windowWidth * 0.48,
  }),
  itemsBackHeader:style.view({
    flexDirection: 'row',
    alignItems: 'flex-end',
    paddingLeft: windowWidth * 0.15,
  }),
  backArrowImage: style.image({
    width: windowWidth * 0.056,
    height: windowWidth * 0.052,
  }),
  headerText: style.text({
    fontSize: windowWidth * 0.044,
    color: Color.white,
    fontFamily: fonts.geometriaBold,
    paddingLeft:  windowWidth * 0.032,
  }),
  backHeaderImage: style.image({
    width: windowWidth * 0.8,
    height: windowWidth * 0.35,
    marginTop: windowWidth * 0.05,
  }),
  buildingType: style.text({
    fontFamily: fonts.geometriaBold,
    fontSize: windowWidth * 0.05,
    color: Color.white,
  }),
  buildingAddress: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.044,
    fontFamily: fonts.openSansRegular,
    color: Color.white,
  }),
  headerInfoAccount: style.view({
    flexDirection: 'row',
  }),
  buildingAccount: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometria,
    color: Color.white,
  }),
  buildingAccountNumbers: style.text({
    paddingTop: windowWidth * 0.025,
    fontSize: windowWidth * 0.044,
    fontFamily: fonts.geometriaBold,
    color: Color.white,
  }),
  contentContainer: style.view({
    flex: 1,
  }),
  interviewContainer: style.view({
    paddingLeft: windowWidth * 0.032,
    borderBottomWidth: windowWidth * 0.0025,
    borderBottomColor: Color.spanishGray,
  }),
  content: style.view({
    flexDirection: 'row',
    alignItems: 'center',
    height: windowWidth * 0.145,
  }),
  billsImage: style.image({
    width: windowWidth * 0.062,
    height: windowWidth * 0.069,
  }),
  countersDrawImage: style.image({
    width: windowWidth * 0.08,
    height: windowWidth * 0.053,
  }),
  supervisorImage: style.image({
    width: windowWidth * 0.08,
    height: windowWidth * 0.07,
  }),
  interviewImage: style.image({
    width: windowWidth * 0.069,
    height: windowWidth * 0.069,
  }),
  contentTextCounters: style.text({
    fontSize: windowWidth * 0.0497,
    fontFamily: fonts.geometriaBold,
    paddingLeft: windowWidth * 0.037,
  }),
  contentText: style.text({
    fontSize: windowWidth * 0.0497,
    fontFamily: fonts.geometriaBold,
    paddingLeft: windowWidth * 0.049,
    paddingRight: windowWidth * 0.43,
  }),
  lineMenuImage: style.image({
    width: '100%',
    height: windowWidth * 0.002,
  }),
  callingImage: style.image({
    width: windowWidth * 0.068,
    height: windowWidth * 0.078,
  }),
  smartHouseImage: style.image({
    width: windowWidth * 0.073,
    height: windowWidth * 0.073,
  }),
  aboutImage: style.image({
    width: windowWidth * 0.074,
    height: windowWidth * 0.068,
  }),
  politicsImage: style.image({
    width: windowWidth * 0.067,
    height: windowWidth * 0.071,
  }),
  fastCodeImage: style.image({
    width: windowWidth * 0.07,
    height: windowWidth * 0.07,
  }),
  footerContainer: style.view({
    backgroundColor: Color.white,
  }),
  footer: style.view({
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft:  windowWidth * 0.02,
  }),
  exitAccountImage: style.image({
    width: windowWidth * 0.075,
    height: windowWidth * 0.07,
  }),
  footerTextView: style.view({
    paddingLeft: windowWidth * 0.05,
  }),
  footerExit: style.text({
    fontSize: windowWidth * 0.05,
    fontFamily: fonts.geometriaBold,
    paddingTop: windowWidth * 0.032
  }),
  footerVersion: style.text({
    fontSize: windowWidth * 0.0259,
    fontFamily: fonts.geometriaLight,
    paddingBottom: windowWidth * 0.034,
    paddingTop: windowWidth * 0.024,
    color: Color.spanishGray,
  }),
  itemsSubparagraphList: style.view({
    backgroundColor: Color.antiFlashWhite,
    height: accountsListItemHight,
    justifyContent: 'center',
    paddingLeft:  windowWidth * 0.19,
    borderBottomWidth: windowWidth * 0.0025,
    borderBottomColor: Color.spanishGray,
  }),
  itemsSubparagraphText:style.text({
    fontSize: windowWidth *  0.039,
    fontFamily: fonts.geometriaMedium,
  }),
  accountsList: style.view({
    overflow: 'hidden',
  }),
  arrowAccountImage: style.image({
    overflow: 'hidden',
    width: windowWidth * 0.026,
    height: windowWidth * 0.019,
  }),
})

const drawerList: IMenuItems[]= [
  {
    image: ImageRepository.countersDraw,
    stylesImage: styles.countersDrawImage,
    text:'Счетчики',
    routeName: 'Login'
  },
  {
    image: ImageRepository.requestListMain,
    stylesImage: styles.supervisorImage,
    text:'Диспетчер',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.votes,
    stylesImage: styles.aboutImage,
    text:'Голосование',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.interview,
    stylesImage: styles.interviewImage,
    text:'Опросы',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.calling,
    stylesImage: styles.callingImage,
    text:'Связь',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.news,
    stylesImage: styles.interviewImage,
    text:'Новости',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.security,
    stylesImage: styles.callingImage,
    text:'Охрана',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.smartHouse,
    stylesImage: styles.smartHouseImage,
    text:'Умнынй дом',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.votes,
    stylesImage: styles.aboutImage,
    text:'О приложении',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.politics,
    stylesImage: styles.politicsImage,
    text:'Политика',
    routeName: 'qqqq'
  },
  {
    image: ImageRepository.fastCode,
    stylesImage: styles.fastCodeImage,
    text:'Быстрый код',
    routeName: 'qqqq'
  },

]