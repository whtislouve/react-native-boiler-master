import React, { PureComponent } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  TextStyle,
} from 'react-native'
import {
  Color,
  fonts,
  ImageRepository,
  style,
  windowWidth,
} from 'app/system/helpers'


interface IProps {
  containerStyle?: TextStyle
  headerDateStyle?: TextStyle
  textHeadStyle?: TextStyle
  textHead?: string
  textDate?: string
  lineImageStyle?: TextStyle
  lineImage?: Image
}

interface IState {

}

export class Block extends PureComponent <IProps, IState> {
  render() {
    const {
      containerStyle,
      headerDateStyle,
      textHeadStyle,
      textHead,
      textDate,
      lineImageStyle,
      lineImage,
    } = this.props

    const textHeadFlatten = StyleSheet.flatten([
      styles.headerPersAcc,
      textHeadStyle,
    ])

    const containerFlatten = StyleSheet.flatten([
      styles.container,
      containerStyle,
    ])

    const headerDataFlatten = StyleSheet.flatten([
      styles.headerDateInfo,
      headerDateStyle
    ])

    const lineFlatten = StyleSheet.flatten([
      styles.line,
      lineImageStyle
    ])
    return (
      <View>
        <View style={containerFlatten}>
          <Text style={textHeadFlatten}>
            {textHead}
          </Text>
          <Text style={headerDataFlatten}>
            {textDate}
          </Text>
        </View>
        <View>
          <Image
            style={lineFlatten}
            source={lineImage}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: style.view({
    paddingLeft: windowWidth * 0.048,
  }),
  headerPersAcc: style.text({
    fontSize: windowWidth * 0.04,
    fontFamily: fonts.geometriaBold,
    color: Color.darkCharcoal
  }),
  headerDateInfo: style.text({
    fontFamily: fonts.geometriaBold,
    color: Color.mediumSeaGreen,
    fontSize: windowWidth * 0.04,
  }),
  line: style.image({
    position: 'absolute',
    top: windowWidth * 0.038,
    right: windowWidth * 0.006,
    width: windowWidth * 0.9,
  }),
})
