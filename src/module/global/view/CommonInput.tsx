import React, { PureComponent } from 'react'
import {
  View,
  TextInput,
  TextInputProps,
  TextStyle,
  ViewStyle,
  Text,
  StyleSheet,
} from 'react-native'
import {
  styleSheetCreate,
  style,
  Color,
  windowWidth,
  fonts,
} from 'app/system/helpers'

interface IProps extends TextInputProps {
  label: string
  textStyle?: TextStyle
  inputStyle?: TextStyle,
  containerStyle?: TextStyle
}

interface IState {

}

export class CommonInput extends PureComponent<IProps,IState>{
  render() {
    const {
      label,
      textStyle,
      inputStyle,
      containerStyle,
    } = this.props

    const containerFlatten = StyleSheet.flatten([
      containerStyle,
      styles.container,
    ])

    const textFlatten = StyleSheet.flatten([
      textStyle,
      styles.text,
    ])

    const inputFlatten = StyleSheet.flatten([
      inputStyle,
      styles.textInput,
    ])

    return (
      <View style={containerFlatten}>
        <TextInput
          style={inputFlatten}
          {...this.props}
        />
        <Text style={textFlatten}>
          {label}
        </Text>
      </View>
    )
  }
}

const styles = styleSheetCreate({
  container: style.view({
    width: windowWidth * 0.97,
    height: windowWidth * 0.13,
    paddingHorizontal: windowWidth * 0.04,
  }),
  textInput: style.text({
    borderRadius: windowWidth * 0.065,
    borderWidth: windowWidth * 0.0025,
    borderColor: Color.darkCharcoal,
    height: '100%',
    paddingHorizontal: windowWidth * 0.05,
  }),
  text: style.text({
    position: 'absolute',
    top: windowWidth * -0.025,
    left: windowWidth * 0.09,
    backgroundColor: Color.white,
    paddingHorizontal: windowWidth * 0.01,
    fontFamily: fonts.geometriaLight,
    fontSize: windowWidth * 0.04,
    color: Color.graniteGray,
  }),
})
