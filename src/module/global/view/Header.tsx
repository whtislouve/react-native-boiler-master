import React, { PureComponent } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  TextStyle,
} from 'react-native'
import {
  Color,
  fonts,
  ImageRepository,
  style,
  windowWidth
} from 'app/system/helpers'
import {DrawerNavigationProp} from '@react-navigation/drawer'

interface IProps {
  isMainHeader?: boolean
  title?: string
  navigation: DrawerNavigationProp<any>
}

interface IState {

}

export class Header extends PureComponent<IProps, IState> {

  sideBar = () => {
    this.props.navigation.openDrawer()
    console.log('aaaaaaaaaaa',this.props.navigation)
  }

  render() {
    return (
       <View>
          <View style={styles.head}>
            <View>
              <Text style={styles.headText}>
                УК «АРСИ КОМФОРТ»
              </Text>
            </View>
            <View>
              <TouchableOpacity>
                <Image
                  style={styles.bellImage}
                  source={ImageRepository.bell}/>
                  <Text style={styles.bellCount}>
                    3
                  </Text>
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={this.sideBar}>
                <Image
                  style={styles.sideMenuImage}
                  source={ImageRepository.sideMenu}/>
              </TouchableOpacity>
            </View>
          </View>
        </View>
    )
  }
}

const styles = StyleSheet.create({
  head: style.view({
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: Color.mediumSeaGreen,
    height: windowWidth * 0.27,
    paddingHorizontal: windowWidth * 0.03,
    paddingTop: windowWidth * 0.1,
  }),
  headText: style.text({
    fontSize: windowWidth * 0.05,
    color: Color.white,
    fontFamily: fonts.geometriaBold,
    paddingRight: windowWidth * 0.11,
  }),
  bellImage: style.image({
    width: windowWidth * 0.094,
    height: windowWidth * 0.1,
  }),
  bellCount: style.text({
    position: 'absolute',
    right: windowWidth * 0.0146,
    top: windowWidth * 0.001,
    color: Color.white,
    fontSize: windowWidth * 0.032,
    fontFamily: fonts.geometriaBold
  }),
  sideMenuImage: style.image({
    width: windowWidth * 0.1,
    height: windowWidth * 0.1,
  }),
})
