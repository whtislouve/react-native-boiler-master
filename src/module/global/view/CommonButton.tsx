import React, { PureComponent } from 'react'
import {
  Text,
  TextStyle,
  TouchableOpacity,
  TouchableOpacityProps,
  ViewStyle,
  StyleSheet,
} from 'react-native'
import {
  styleSheetCreate,
  style,
  windowWidth,
  Color,
  fonts,
} from 'app/system/helpers'


interface IProps extends TouchableOpacityProps {
  label: string,
  containerStyle?: ViewStyle,
  textStyle?: TextStyle,
  color?: any,
}

interface IState {

}

const colors = {
  PURPLE: Color.royalPurple,
  GREEN: Color.mediumSeaGreen,
}

export class CommonButton extends PureComponent<IProps, IState>{
  render() {
    const {
      label,
      containerStyle,
      color,
      textStyle,
    } = this.props

    const containerFlatten = StyleSheet.flatten([
      styles.container,
      containerStyle,
      {
        backgroundColor: color ? colors[color] : Color.mediumSeaGreen
      }
    ])

    const textFlatten = StyleSheet.flatten([
      textStyle,
      styles.text,
    ])
    return (
      <TouchableOpacity
        {...this.props}
        style={containerFlatten}
      >
        <Text style={textFlatten}>
          {label}
        </Text>
      </TouchableOpacity>
    )
  }
}

const styles = styleSheetCreate({
  container: style.view({
    width: windowWidth * 0.44,
    height: windowWidth * 0.13,
    borderRadius: windowWidth * 0.067,
  }),
  text: style.text({
    alignSelf: 'center',
    paddingTop: windowWidth * 0.04,
    fontSize: windowWidth * 0.039,
    fontFamily: fonts.geometriaBold,
    color: Color.white,
  }),
})

